# Acerca de

**De Ludo Bellico, **latín para expresar el concepto de "juego de guerra" (Gracias a toda la comunidad de [Punta de Lanza](http://www.puntadelanza.net/Index.php) por ayudarme con el título,Gorgias96 por la idea de usar latín y Piteas por sugerirme la forma latina _de + ablativo_ ) 

> 1.-  'dê' : "de, desde". (preposición). , 2.-  'lûdus,î' : "juego". (sustantivo). 2ª Declinación 3.-  'bellicus,a,um' : "militar; guerrero". (adjetivo). 2ª Adj.

De Ludo Bellico, a partir de ahora **DLB**, es un juego de guerra desarrollado en Java y con la interfaz gráfica en Swing. En este blog iré contando los avances en el desarrollo del juego, pero no sólo eso, sino que además incluiré artículos didácticos sobre diferentes aspectos de la concepción, diseño y programación de juegos de guerra. Las características generales del juego son 

  * Mapa hexagonal con amplia variedad de terrenos
  * Simulación bélica a nivel operacional, aunque con capacidad para representar diferentes escalas tanto espaciales como temporales
  * Dinámica de juego WeGo
  * Base de datos con equipamiento de todo el siglo XX, con capacidad para modificar la base de datos o usar bases de datos específicas de un escenario en concreto
  * Compatible con escenarios del TOAW III (mediante una herrramienta de importación)
Las siguientes secciones detallan las características planeadas para el juego 

## El mapa

  * El mapa que caracteriza el terreno de juego o tablero, sigue una configuración de teselado hexagonal, es decir, se componen de celdas de forma hexagonal.
  * Cada celda puede contener una combinación de tipos de terreno, incluyendo accidentes geográficos (montañas, ríos, ramblas, acantilados, marismas, etc.), tipos de suelo y vegetación (desiertos, rocas, bosques, campos de cultivo, etc. ), y poblaciones de diferente tamaño.
  * Infraestructuras, como fortificaciones, carreteras y vías de ferrocarril, puertos y aeropuertos. Algunas infraestructuras, como puentes y vías, se pueden destruir y reparar dinámicamente.
  * Utilización de gráficos con transiciones entre hexágonos, es decir, la presencia de un determinado tipo de terreno en un hexágono se muestra con diferente apariencia según los tipos de terreno existentes en los hexágonos adyacentes.
  * Soporte para diferentes escalas: en principio se puede definir cualquier escala, aunque el tipo motor de juego que se está desarrollando funciona mejor en un rango entre los 2'5 y los 25 Km

## Las unidades

  * En un escenario de juego puede participar un número arbitrario de **fuerzas**.
  * Cada fuerza se componen de una colección de **unidades**, las cuales pueden realizar acciones sobre el terreno de juego. Estas unidades tienen una representación gráfica sobre el tablero, utilizando símbolos militares apropiados al tipo de unidad y diferentes combinaciones de color según el bando o formación a la cual pertenecen.
  * Las unidades siguen una organización jerárquica definido por el Orden de Batalla de la fuerza a la cual pertenecen. En concreto, cada unidad pertenece a una y sólo una **formación**,** **la cual puede estar subordinada a otra formación, conformando así la cadena de mando.

## El motor de juego

  * Motor de juego en **tiempo real pausable**. Admite el modo **WeGo**: los jugadores dan órdenes por turnos, pero la ejecución y resolución de las órdenes ocurre de forma simultánea.
Nota: El modo WeGo es una solución intermedia entre los juegos por turnos clásicos, en los cuales los jugadores siguen turnos estrictamente secuenciales (primero juega uno, luego otro, de ahí que a esta modalidad se la conozca también como IGoYouGo), y los juegos de tiempo real. En el modo WeGo, las acciones se ejecutan de forma simultánea para todos los jugadores, como en el tiempo real, pero se definen una serie de turnos periódicos en los cuales los jugadores pueden dar órdenes. Si te interesa el código del proyecto puedes consultarlo así como contribuir a su desarrollo. Lo encontrarás en [bitbucket](https://bitbucket.org/magomar/ares)